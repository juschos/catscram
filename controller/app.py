from flask import Flask, render_template, redirect, url_for, request, jsonify, abort
from gpiozero import LED
from turbo_flask import Turbo
import threading
import time

#led = LED(16)
led = LED(23, active_high=False) # This is for a relay with inverted input.
# relay-pin map 1 = 23, 2 = 24, 3 = 22, 4 = 27
app = Flask(__name__)

def activate(time=20):
  led.blink(on_time=time, n=1, background=True)

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/on')
def set_on():
    activate(time=30)
    return redirect(url_for('index'))


@app.route('/off')
def set_off():
    led.off()
    return redirect(url_for('index'))


@app.route('/has-api/', methods=["GET","POST"])
def has_api():
    if request.method == 'GET':
        return jsonify(active=led.is_active)

    if request.method == 'POST':
        state=request.data.decode('UTF-8').lower()
        if state == 'on':
            activate(time=300)
            return jsonify(state)
        if state == 'off':
            led.off()
            return jsonify(state)
    abort(400)


@app.context_processor
def inject_load():
    status = 'On' if led.is_active else 'Off'
    return {'status': status}


if __name__ == "__main__":
    app.run()
