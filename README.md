# Catscram

### A very simple project consisting of two parts which can be used seperatly.

1. Detector: Yolov5 Object detection via a ip camera, taking an activate or deactivate action based on what is being detected

2. Controller: Flask webserver on a raspberri pi activating or deactivating a pin based on a url or api call

## Circuit
In my setup I have connected the rasberri pi pin to a simple circuit, 
that opens a NC Water Solenoid Valve when the pin is on.

```
   +--------------------------+
   |                          |
   |  ______________         LOAD
5v-|-|     RPI     |          |
     |             |         /
     |      pin(x) |--/\/\--| 2n2222 
     |_____________|  220R   \
      |                       |
     ===         GND         ===
                         
```

## Uses

This enables me to turn on sprinklers for my garden via the webserver on the RPI via the site, via the detector software, or HomeAssistant.
It is essentially a very rudimentary sprinkler api 


It has some other benefits too:


![CSDemo-tiny](/uploads/9f9abddb5f12c371b40c1a77ef03c4fd/CSDemo-tiny.gif)


## Hardware used:

1. [Raspberri PI Zero](https://www.raspberrypi.org/products/raspberry-pi-zero/)

2. [Reolink RLC-520](https://reolink.com/gb/product/rlc-520/)

3. For the Solenoid I had [This](https://www.primrose.co.uk/jet-spray-cat-repeller-pestbye-p-1907.html) lying around, so I took the solenoid out of it. I think the only thing that really matters is that it can open with 5v. This one drew too much current for the pi, hence the circuit.


### UDATE: V2 hardware

The original plastic Solenoid started leaking, so I went with a better brass one, but it required more volts to open.
I replaced the simple circuit with a relay passing 12v DC, controlled by the same pin oon the raspberri pi.


## HomeAssistant
Because the controller can be controlled via an api, it can be used in HomeAssistant
To use the controller as an entity in HomeAssistant, add the following to your HomeAssistant's config.yaml

```
switch:
  - platform: rest
    name: 'Catscram - spray'
    resource: http://10.0.0.1:5000/api/
    body_on: 'on'
    body_off: 'off'
    is_on_template: '{{ value_json.active }}'
```

This will create an entity which can be used by a button card in the UI
